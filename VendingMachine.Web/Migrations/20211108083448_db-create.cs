﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VendingMachine.Web.Migrations
{
    public partial class dbcreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "vm");

            migrationBuilder.CreateTable(
                name: "Coins",
                schema: "vm",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Count = table.Column<int>(type: "int", nullable: false),
                    Cost = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coins", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Drinks",
                schema: "vm",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(1024)", maxLength: 1024, nullable: true),
                    Count = table.Column<int>(type: "int", nullable: false),
                    Cost = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Drinks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                schema: "vm",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TimeStamp = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Summa = table.Column<int>(type: "int", nullable: false),
                    DrinkId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Drinks_DrinkId",
                        column: x => x.DrinkId,
                        principalSchema: "vm",
                        principalTable: "Drinks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                schema: "vm",
                table: "Coins",
                columns: new[] { "Id", "Cost", "Count", "Status" },
                values: new object[,]
                {
                    { 1, 1, 10, true },
                    { 2, 2, 10, true },
                    { 3, 5, 10, false },
                    { 4, 10, 10, true }
                });

            migrationBuilder.InsertData(
                schema: "vm",
                table: "Drinks",
                columns: new[] { "Id", "Cost", "Count", "Name", "Picture", "Status" },
                values: new object[,]
                {
                    { 1, 3, 10, "Coca-cola", null, true },
                    { 2, 5, 10, "Fanta", null, true },
                    { 3, 4, 10, "Sprite", null, false },
                    { 4, 7, 10, "Pepsi", null, true }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Drinks_Name",
                schema: "vm",
                table: "Drinks",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_DrinkId",
                schema: "vm",
                table: "Orders",
                column: "DrinkId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Coins",
                schema: "vm");

            migrationBuilder.DropTable(
                name: "Orders",
                schema: "vm");

            migrationBuilder.DropTable(
                name: "Drinks",
                schema: "vm");
        }
    }
}
