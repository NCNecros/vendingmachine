﻿using FluentValidation;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using VendingMachine.Models.Dto;
using VendingMachine.Models.Models;

namespace VendingMachine.Web.Validators
{
    public class DrinkRequestDtoValidator : AbstractValidator<DrinkRequestDto>
    {
        public DrinkRequestDtoValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.Name).MaximumLength(255);
        }
    }
}
