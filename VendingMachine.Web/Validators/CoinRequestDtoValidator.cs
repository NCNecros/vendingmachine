﻿using FluentValidation;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using VendingMachine.Models.Dto;
using VendingMachine.Models.Models;

namespace VendingMachine.Web.Validators
{
    public class CoinRequestDtoValidator :AbstractValidator<CoinRequestDto>
    {
        public CoinRequestDtoValidator()
        {
            RuleFor(x => x.Count).InclusiveBetween(0, 255);
            RuleFor(x => x.Id).NotEmpty();
        }

    }
}
