﻿using AutoMapper;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using VendingMachine.DAL.Services;
using VendingMachine.Models.Context;
using VendingMachine.Models.Dto;
using VendingMachine.Models.Models;

namespace VendingMachine.Web.Controllers
{
    /*
     * 1 изменить количетсво монет и их состояние от админа
     * 2 выдать количество хранящихся монет фронту при заказе
     */

    [ApiController]
    [Route("[controller]")]
    public class CoinController : ControllerBase
    {
        private readonly ILogger<CoinController> _logger;
        private readonly IMapper _mapper;
        private readonly ICoinService coinService;


        [HttpPut("{id:int}")]
        public  async Task<ActionResult<CoinResponseDto>> UpdateCoin(int id, [FromBody] CoinRequestDto coinRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.Values);
            }
            var coin = await coinService.FindAsync(id);
            if (coin == null)
            {
                return BadRequest(new { Error = "Не удалось найти Coin" });
            }
            try
            {
                coin.Count = coinRequest.Count;
                coin.Status = coinRequest.Status;
                await coinService.SaveAsync(coin);
            }
            catch (Exception e)
            {
                return Problem(e.Message); 
            }
            return Ok(_mapper.Map<CoinResponseDto>(coin));
        }

        [HttpGet("GetAll")]
        public IEnumerable<CoinResponseDto> GetAll()
        {
            return coinService.GetAll().Select(e=>_mapper.Map<CoinResponseDto>(e));
        }

        public CoinController(ILogger<CoinController> logger, IMapper mapper, ICoinService coinService)
        {
            _logger = logger;
            _mapper = mapper;
            this.coinService = coinService;
        }
    }
}