﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using VendingMachine.Models.Models;
using VendingMachine.Models.Dto;
using VendingMachine.Models.Context;

namespace VendingMachine.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly VmContext _db;
        public WeatherForecastController(ILogger<WeatherForecastController> logger, VmContext db)
        {
            _logger = logger;
            this._db = db;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpPost]
        public IActionResult Post([FromBody] CoinRequestDto coin)

        {

            //Надо поставить Automapper, настроить мэппинги и тогда жить будет проще и веселее

            //Мэппим Dto на сущность, потом пишем ее в базу и возвращаем результат
            var c = new Coin();
            c.Count = coin.Count;
            var result = _db.Coins.Add(c);
            _db.SaveChanges();

            return Created("Post", coin); //вернуть надо CoinResponseDto
        }
}
}
