﻿using AutoMapper;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using VendingMachine.DAL.Services;
using VendingMachine.Models.Dto;
using VendingMachine.Models.Models;

namespace VendingMachine.Web.Controllers
{


    [ApiController]
    [Route("[controller]")]
    public class DrinkController : ControllerBase
    {
        private readonly ILogger<DrinkController> _logger;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IMapper _mapper;
        private readonly IDrinkService drinkService;
        [HttpGet]
        [Route("GetAll")]
        public IEnumerable<DrinkResponseDto> GetAll()
        {
            return drinkService
                .GetAll()
                .Select(e => _mapper.Map<DrinkResponseDto>(e));
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete (int id){
            try{
            await drinkService.DeleteAsync(id);
            return Ok();
            }catch{
                return Problem();
                
            }
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<DrinkResponseDto>> Update(int id, [FromForm] DrinkRequestDto drinkRequest)
        {
            var drink = await drinkService.FindAsync(id);
            drink.Name = drinkRequest.Name;
            drink.Cost = drinkRequest.Cost;
            drink.Count = drinkRequest.Count;
            drink.Status = drinkRequest.Status;
            if (drinkRequest.ImageFile != null)
            {
                var file = drinkRequest.ImageFile;

                if (file.Length > 0)
                {
                    var fullPath = SaveFile(file);
                    drink.Picture = fullPath.Result;
                }
            }


            if (drinkService.SaveAsync(drink) != null)
            {
                return Ok(_mapper.Map<DrinkResponseDto>(drink));
            }
            else
            {
                return Problem();
            }

        }

        public DrinkController(ILogger<DrinkController> logger, IWebHostEnvironment webHostEnvironment, IMapper mapper, IDrinkService drinkService)
        {
            _logger = logger;
            _webHostEnvironment = webHostEnvironment;
            _mapper = mapper;
            this.drinkService = drinkService;
        }

        private async Task<string> SaveFile(IFormFile file)
        {
            var randomFilename = Path.GetFileNameWithoutExtension(Path.GetRandomFileName());
            var filename = Path.Combine(randomFilename + Path.GetExtension(file.FileName));
            var imagePath = Path.Combine(_webHostEnvironment.ContentRootPath, "ClientApp/Public/Images");
            var fullPath = Path.Combine(imagePath, filename);
            var fullPathForDB = Path.Combine(".\\images", filename);
            if (!Directory.Exists(imagePath))
            {
                Directory.CreateDirectory(imagePath);
            }
            await using (var stream = System.IO.File.Create(fullPath))
            {
                await file.CopyToAsync(stream);
            }
            return fullPathForDB;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromForm] DrinkCreateDto drinkCreateDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.Values);
            }
            var drink = _mapper.Map<Drink>(drinkCreateDto);
            var contentRootPath = _webHostEnvironment.ContentRootPath;
            var randomFilename = Path.GetFileNameWithoutExtension(Path.GetRandomFileName());
            if (drinkCreateDto.ImageFile != null)
            {
                var file = drinkCreateDto.ImageFile;
                if (file.Length > 0)
                {
                    try
                    {
                        drink.Picture = await SaveFile(drinkCreateDto.ImageFile);
                    }
                    catch
                    {
                        return Problem();
                    }

                }
            }
            await drinkService.AddNewAsync(drink);
            return Ok(_mapper.Map<DrinkResponseDto>(drink));
        }
    }
}