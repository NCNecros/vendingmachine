﻿using AutoMapper;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using VendingMachine.DAL.Services;
using VendingMachine.Models.Context;
using VendingMachine.Models.Models;
using VendingMachine.Models.Dto;
using Microsoft.AspNetCore.Authentication;

namespace VendingMachine.Web.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly ILogger<OrderController> _logger;
        private readonly IOrderService _orderService;
        private readonly IDrinkService _drinkService;
        private readonly ICoinService _coinService;
        private readonly IMapper _mapper;
        public OrderController(ILogger<OrderController> logger, IMapper mapper,
            IOrderService orderService, IDrinkService drinkService, ICoinService coinService)
        {
            _logger = logger;
            _orderService = orderService;
            _drinkService = drinkService;
            _mapper = mapper;
            _coinService = coinService;
        }

        [HttpPost]
        public async Task<ActionResult> SaveOrder([FromBody] OrderRequestDto orderRequestDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.Values);
            }
            var summa = orderRequestDto.Drinks.Sum(e => e.Cost * e.Count);
            var order = await _orderService.SaveAsync(_mapper.Map<Order>(orderRequestDto), summa);
            var drinkSavedSuccess = await _drinkService.ChangeCountAsync(orderRequestDto.Drinks);
            await _coinService.ChangeCoinAsync(orderRequestDto.Coins);
            if (order != null && drinkSavedSuccess)
            {
                OrderResponseDto orderResponse = new OrderResponseDto();
                orderResponse.Result = true;
                orderResponse.Coins = getRest(orderRequestDto.Balance);
                return Ok(orderResponse);
            }
            else
            {
                return Problem();
            }

        }

        private List<CoinResponseDto> getRest(int rest)
        {
            var result = _coinService.GetAll().Select(e => _mapper.Map<CoinResponseDto>(e)).OrderBy(e => e.Cost).Reverse().ToList();
            var _rest = rest;
            foreach (CoinResponseDto coin in result)
            {
                coin.Count = 0;
                if (_rest >= coin.Cost)
                {
                    int wholePart = _rest / coin.Cost;
                    _rest -= wholePart * coin.Cost;
                    coin.Count=wholePart;
                }
            }

            return result;

        }
    }
}
