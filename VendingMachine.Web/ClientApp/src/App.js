import { BrowserRouter as Router, Route, Switch, useLocation} from "react-router-dom";

import { CoinList } from "./components/coin-list";
import { DrinkList } from "./components/drink-list";
import { Order } from "./components/order";
import AdminCoinList from "./components/admin/admin-coin-list";
import AdminDrinkList from "./components/admin/admin-drink-list";

import "./styles.css";
import AdminAddDrink from "./components/admin/admin-add-drink";

export default function App() {
    const query = useQuery();
    return (
        <div className="App">
            <Router>
                <Switch>
                    <Route path='/' exact component={() => <ClientPage />} />
                    <Route path='/admin' exact component={() => <AdminPage secret={query.get("secret")} />} />
                </Switch>
            </Router>
        </div>
    );
}

const ClientPage = () => {
    return (
        <div className="client-page">
            <h1>Sale of drinks</h1>
            <h2> Insert coin </h2>
            <CoinList />
            <DrinkList />
            <Order />
        </div>
    );
}
const AdminPage = ({ secret }) => {
    if (secret !== '123') {
        return (
            <div><h1>Access Denied</h1></div>
            );
    }
    return (
        <div className="admin-page">
            <h1>Admin Page</h1>
            <AdminAddDrink />
            <AdminCoinList />
            <AdminDrinkList />
        </div>
    );
}
function useQuery() {
    return new URLSearchParams(useLocation().search);
}