﻿import React from "react";
import "./coin-list-item.css";
import { useDispatch } from 'react-redux';
import { addCoin } from "../../actions/coin-actions";

function CoinListItem({ id, cost, status}) {
    const dispatch = useDispatch();
    return (
        <div className="col-sm-3">
            <button
                type="button"
                disabled={!status}
                className="coin-list-item btn btn-success"
                onClick={()=>dispatch(addCoin(id))}>
                {cost}
            </button>
        </div>
    );
}

export { CoinListItem };