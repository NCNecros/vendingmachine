﻿import React from 'react';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchCoins } from '../../../actions/coin-actions';
import AdminCoinListItem from '../admin-coin-list-item';
import './admin-coin-list.css';

const AdminCoinList = () => {
    const state = useSelector((state) => state.coin);
    const dispatch = useDispatch();
    useEffect(
        () => dispatch(fetchCoins()),[]
    );
    const coinList = state.coins.map((el) => {
        return (
            <AdminCoinListItem key={el.id} {...el} />
        );
    });
    return (
        <div className="row admin-coin-list">
            {coinList}
        </div>
        );
}

export default AdminCoinList;