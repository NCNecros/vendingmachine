﻿import React from 'react';
import './admin-coin-list-item.css';
import { useDispatch } from 'react-redux';
import {adminIncCoin, adminDecCoin, adminToggleStatus} from "../../../actions/coin-actions"

const AdminCoinListItem = ({ cost,id, count, status }) => {
    const dispatch = useDispatch();

    return (
        <div className="col-sm-3">
            <table className="coin-table">
                <tbody>
                    <tr>
                        <td rowSpan="2" className="text-center coin-name">{cost}<br/> coin</td>
                        <td><button onClick={()=>dispatch(adminDecCoin(id))} className="btn btn-danger btn-sm"><i className="fa fa-minus-square" aria-hidden="true"></i>
                        </button></td>
                        <td>{count}</td>
                        <td><button onClick={()=>dispatch(adminIncCoin(id))} className="btn btn-success btn-sm"><i className="fa fa-plus-square" aria-hidden="true"></i>
                        </button></td>
                    </tr>
                    <tr>
                        <td colSpan="3" className="text-center">
                            <div>
                                <button onClick={() => dispatch(adminToggleStatus(id))} className="btn btn-light btn-sm">{status? "Enabled":"Disabled"}</button>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        );
}


export default AdminCoinListItem;
