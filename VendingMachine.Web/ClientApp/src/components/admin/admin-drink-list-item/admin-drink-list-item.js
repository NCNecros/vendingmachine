﻿import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import "./admin-drink-list-item.css";
import {
    deleteDrink,saveDrinkEdit
} from "../../../actions/drink-actions";

import 'react-edit-text/dist/index.css';
import { useState } from 'react';



const AdminDrinkListItem = ({ id }) => {
    const dispatch = useDispatch();
    const state = useSelector((state) => state.drink);
    const item = state.drinks.find((el) => el.id === id);
    const [name, setName] = useState(item.name);
    const [count, setCount] = useState(item.count);
    const [cost, setCost] = useState(item.cost);
    const [status, setStatus] = useState(item.status);
    const [imageFile, setImageFile] = useState();
    let drinkImage = null;
    if (item.picture == null) {
        drinkImage = './placeholder.png';
    } else {
        drinkImage = item.picture;
    }
    const onChangeCost = (event) => {
        setCost(event.target.value);
    }
    const onChangeCount = (event) => {
        setCount(event.target.value);
    }
    const onChangeName = (event) => {
        setName(event.target.value);
    }
    const toggleStatus = () => {
        setStatus(!status);
    }
    const uploadFile=(event) => {
        setImageFile(event.target.files[0]);
    }
    const onSaveDrink = () => {
        console.log(status);
        const data = {id, name, cost, count, imageFile, status}
        dispatch(saveDrinkEdit(data));
    }
    return (
        <AdminDrinkListItemContent
            id={id}
            name={name}
            cost={cost}
            count={count}
            status={status}
            drinkImage={drinkImage}
            dispatch={dispatch}
            onChangeCost={onChangeCost}
            onChangeCount={onChangeCount}
            onChangeName={onChangeName}
            toggleStatus={toggleStatus}
            uploadFile={uploadFile}
            onSaveDrink={onSaveDrink}
        />
    );
}
const AdminDrinkListItemContent =
    ({ id, name, cost, count, status, dispatch,
        drinkImage, onChangeCost, onChangeCount, onChangeName,
        toggleStatus, uploadFile, onSaveDrink
    }) => {

        return (
            <div className="row admin-drink-list-item">
                <div className="col-sm-12">
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td rowSpan="3" width="140px"><img alt="loading..." src={drinkImage} /></td>
                                <td colSpan="2" width="60%">
                                    <label htmlFor="drinkName">Name: </label>
                                    <input id="drinkName" type="text" value={name} onChange={(event) => onChangeName(event)} />
                                </td>
                                <td><button onClick={() => onSaveDrink()} className="btn btn-success">Save</button></td>
                            </tr>
                            <tr>
                                <td>
                                    <label htmlFor="drinkCount">Count: </label>
                                    <input id="drinkCount" type="number" value={count} onChange={(event) => onChangeCount(event)} />
                                </td>
                                <td>
                                    <label htmlFor="drinkCost">Cost: </label>
                                    <input id="drinkCost" type="number" value={cost} onChange={(event) => onChangeCost(event)} />
                                </td>
                                <td><button onClick={() => dispatch(deleteDrink(id))} className="btn btn-danger ">Delete</button></td>
                            </tr>
                            <tr>

                                <td colSpan="2">
                                    <button onClick={() => toggleStatus()} className="btn btn-light btn-outline-dark">{status ? "Enabled" : "Disabled"}</button>
                                </td>
                                <td>
                                    <label className="drink-img-upload">
                                        <input type="file" onChange={uploadFile} accept=".jpg,.jpeg,.png" />
                                        Upload
                                    </label>

                                </td>
                            </tr>
                        </tbody>

                    </table>

                </div>
            </div>
        );
    }

export default AdminDrinkListItem;