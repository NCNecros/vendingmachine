﻿import React, {useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import AdminDrinkListItem from "../admin-drink-list-item/";
import { fetchDrinks } from "../../../actions/drink-actions.js";

const AdminDrinkList = () => {
    const state = useSelector((state) => state.drink);
    const drinks = state.drinks;
    const dispatch = useDispatch();
    useEffect(
        () => dispatch(fetchDrinks()),
        [state.drink]);
    let drinkList = null;
    if (drinks !== undefined) {
        const elements = drinks.map((el) => {
            return (
                <AdminDrinkListItem key={el.id} id={el.id}/>
            );

        });
        drinkList = <div className="list-group">{elements}</div>;
    }
    return (
        <div className="list-group admin-drink-list">
            {drinkList}
        </div>
        );
}

export default AdminDrinkList;