﻿import React, { useState } from "react";
import './admin-add-drink.css';
import { useDispatch } from "react-redux";
import { addNewDrink } from '../../../actions/drink-actions';

const AdminAddDrink = () => {
    const [drinkName, setDrinkName] = useState("");
    const [drinkCost, setDrinkCost] = useState("");
    const [drinkCount, setDrinkCount] = useState("");
    const [imageFile, setImageFile] = useState();
    const dispatch = useDispatch();
    const onSaveDrink = (e) => {
        e.preventDefault();
        dispatch(addNewDrink(drinkName, drinkCost, drinkCount, imageFile));
        setDrinkName("");
        setDrinkCost("");
        setDrinkCount("");
        setImageFile(undefined);
    }
    const onNameChange = (value) => {
        setDrinkName(value.target.value);
    }
    const onCostChange = (value) => {
        setDrinkCost(value.target.value);
    }
    const onCountChange = (value) => {
        setDrinkCount(value.target.value);
    }
    const uploadFile=(value) => {
        setImageFile(value.target.files[0]);
    }


    return (
        <div className="row admin-add-drink">
        <div className="col-sm-12">
            <form onSubmit={(e) => onSaveDrink(e)}>
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <input onChange={(value) => onNameChange(value)} value={drinkName} type="text" className="form-control" placeholder="Enter drink name" />
                            </td>
                            <td>
                                <button type="submit" className="btn btn-success">Save</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input onChange={(value) => onCountChange(value)} value={drinkCount} type="number" className="form-control" placeholder="Count" />
                            </td>
                            <td rowSpan="2">
                                <label className="drink-img-upload">
                                    <input type="file" onChange={uploadFile} accept=".jpg,.jpeg,.png" />
                                    Upload
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input onChange={(value) => onCostChange(value)} value={drinkCost} type="number" className="form-control" placeholder="Cost" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
        </div>
    );
}

export default AdminAddDrink;