﻿import React, { useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";
import { addDrink, getStatus } from "../../actions/drink-actions";
import "./drink-list-item.css";

function DrinkListItem({ id, name, cost, count, status, picture  }) {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();
    let drinkImage = null;
    if (picture == null) {
        drinkImage = './placeholder.png';
    } else {
        drinkImage = picture;
    }
    useEffect(
        () => dispatch(getStatus(id)), [state.order]);
    return (
        <DrinkListItemContent id={id} name={name} cost={cost} count={count} status={status} picture={drinkImage} dispatch={useDispatch()} />
    );
}

const DrinkListItemContent = ({ id, name, cost, count, status,picture, dispatch})=>{
return (
    <table className="drink-card">
            <tbody>
                <tr>
                    <td rowSpan="3" width="20%">
                        <img alt="loading..."  src={picture}/>
                    </td>
                    <td width="40%">
                        <b>{name}</b>
                    </td>
                    <td rowSpan="3">
                        <button className="btn btn-success" disabled={!status} onClick={() => dispatch(addDrink(id))}>
                            Add to order
                        </button>
                    </td>
                </tr>
                <tr>
                    <td>Cost: {cost}</td>
                </tr>
                <tr>
                    <td>Count: {count}</td>
                </tr>
            </tbody>
        </table>
);
}
export { DrinkListItem }