﻿import React from "react";
import "./order.css";
import { sendOrder } from '../../actions/order-actions';
import { connect } from 'react-redux';

function Order(props) {
    const order = props.order;

    const drinkList = order.drinks.filter((el) => Object.keys(el).length !== 0).map(({ name, id, count }) => {
        return (
            <div key={id}>
                {name}, {count} шт;
            </div>
        );
    });
    let restList = null;
    if (order.rest != undefined && order.rest.coins != undefined) {
        restList = order.rest.coins.filter((el) => Object.keys(el).length !== 0).filter((el) => el.count>0).map(({ id, cost, count }) => {
            return (
                <div key={id}>
                    Coin {cost}: {count}
                </div>
            );
        });
    }
    

    return (
        <OrderContent
            drinkList={drinkList}
            balance={order.balance}
            sendOrder={sendOrder}
            restList={restList}
            dispatch={props.dispatch}
            cost={order.cost} />
    );

}
const mapStateToProps = (state) => {
    return (
        {
            order: state.order
        }
    );
}
const OrderContent = ({ drinkList, balance, sendOrder, restList, dispatch, cost }) => {
    return (
        <div className="order">
            <div className="row">
                <div className="col-sm-12">
                    Amount: {cost}
                    <br />
                    Ordered: {drinkList}<br/> Remaining: {balance}
                </div>
            </div>
            <div className="row">
                <div className="col-sm-12">
                    <button className="btn btn-success" onClick={() => dispatch(sendOrder())}>Make an order</button>
                </div>
            </div>
            <div className="row">
                <div className="col-sm-12">
                Odd money: {restList}
                </div>
            </div>

        </div>
    );
}
export default connect(mapStateToProps)(Order);