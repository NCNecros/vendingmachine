﻿import React, { useEffect } from "react";
import { CoinListItem } from "../coin-list-item";
import "./coin-list.css";
import { fetchCoins } from '../../actions/coin-actions.js';
import { connect } from "react-redux";
const CoinList = (props) => {

    useEffect(
        () => props.dispatch(fetchCoins()), []);
    let listContent = null;
    if (props.coins !== undefined) {
        const elements = props.coins.map((el) => {
            return (
                <CoinListItem
                    key={el.id}
                    {...el}
                />
            );
        });
        listContent = <CoinListContent elements={elements} />
    }
    const content = props.loading ? <Loading /> : listContent
    return (
        content
    );
}

const Loading = () => {
    return (
        <div className="spinner-border" role="status">
            <span className="sr-only"></span>
        </div>
    );
}

const CoinListContent = ({ elements }) => {
    return (
        <div className="row coin-list">
            {elements}
        </div>
    );
}

const mapStateToProps = (state) => {
    return ({
        coins: state.coin.coins,
        loading: state.coin.loading
    })
};

export default connect(mapStateToProps)(CoinList);