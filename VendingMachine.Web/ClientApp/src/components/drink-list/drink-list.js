﻿import React, { useEffect } from "react";
import { DrinkListItem } from "../drink-list-item";
import { connect } from 'react-redux'
import { fetchDrinks } from '../../actions/drink-actions';

function DrinkList(props) {

    useEffect(
        () => props.dispatch(fetchDrinks()), []);

    let drinkList = null;
    if (props.drinks != undefined) {
        const elements = props.drinks.map((el) => {
            return (
                <DrinkListItem
                    key={el.id}
                    {...el}
                />
            );

        });
        drinkList = <div className="list-group">{elements}</div>;
    }
    const content = props.loading ? <Loading /> : drinkList;
    return content;
}


const Loading = () => {
    return (
        <div className="spinner-border" role="status">
            <span className="sr-only"></span>
        </div>
    );
}

const mapStateToProps = (state) => {
    return ({
        drinks: state.drink.drinks,
        loading: state.drink.loading
    });
}
export default connect(mapStateToProps)( DrinkList );