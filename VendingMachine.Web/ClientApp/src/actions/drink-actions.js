﻿import {
    REQUEST_DRINK,
    DELETE_DRINK, MINUS_DRINK_ADMIN,
    MINUS_DRINK, PLUS_DRINK_ADMIN,
    MINUS_COST_DRINK_ADMIN, EDIT_NAME_IN_STATE, 
    TOGGLE_DRINK_STATUS, ADD_NEW_DRINK, SAVE_DRINK_EDIT
} from "../app/types";

import DrinkService from "../services/drink-service";
import OrderService from '../services/order-service';

const drinkService = new DrinkService();
const orderService = new OrderService();
const addDrink = (id) => {
    return (dispatch, getState) => {
        dispatch(minusDrinkFromStore(getState(), id));
        dispatch(addDrinkToOrder(getState(), id));
    }
};

const addDrinkToOrder = (state, id) => {
    return {
        type: 'ORDER_ADD_DRINK',
        payload: orderService.addDrink(state, id)
    }
}

const plusDrinkAdminFromStore = (state) => {
    return {
        type: PLUS_DRINK_ADMIN,
        payload: state
    }
}

const plusDrinkAdmin = (id) => {
    return (dispatch, getState) => {
        const newState = drinkService.plusDrinkAdmin(getState(), id);
        dispatch(plusDrinkAdminFromStore(newState));
    }
}
const toggleDrinkStatusInState = (state) => {
    return {
        type: TOGGLE_DRINK_STATUS,
        payload: state
    }
}
const toggleDrinkStatus = (id) => {
    return (dispatch, getState) => {
        const newState = drinkService.toggleDrinkStatus(getState(), id);
        dispatch(toggleDrinkStatusInState(newState));
    }
}


const minusDrinkAdminFromStore = (state) => {
    return {
        type: MINUS_DRINK_ADMIN,
        payload: state
    }
}

const uploadFile = (value, id) => {
    return async (dispatch, getState) => {
        try {
            const formData = new FormData(value.target.files[0]);
            const url = 'drink/upload';
            await fetch(url, {
                method: "POST",
                body: formData
            });
        } catch (e) {
            console.error(e);
        }
    }
}

const minusDrinkAdmin = (id) => {
    return (dispatch, getState) => {
        const newState = drinkService.minusDrinkAdmin(getState(), id);
        dispatch(minusDrinkAdminFromStore(newState));
    }
}

const plusCostDrinkAdmin = (id) => {
    return (dispatch, getState) => {
        const newState = drinkService.plusCostDrinkAdmin(getState(), id);
        dispatch(plusDrinkAdminFromStore(newState));
    }
}
const minusCostDrinkAdminFromStore = (state) => {
    return {
        type: MINUS_COST_DRINK_ADMIN,
        payload: state
    }
}

const minusCostDrinkAdmin = (id) => {
    return (dispatch, getState) => {
        const newState = drinkService.minusCostDrinkAdmin(getState(), id);
        dispatch(minusCostDrinkAdminFromStore(newState));
    }
}

const minusDrinkFromStore = (state, id) => {
    return {
        type: MINUS_DRINK,
        payload: drinkService.minusDrink(state, id)
    }
}

const getStatus = (id) => {
    return (dispatch, getState) => {
        dispatch(getStatusFromOrder(getState(), id));
    }
}

const getStatusFromOrder = (state, id) => {
    return {
        type: 'CHECK_DRINK_STATUS',
        payload: drinkService.getStatus(state, id)
    }
}
const editDrinkNameInState = (state) => {
    return {
        type: EDIT_NAME_IN_STATE,
        payload: state
    }
}
const editDrinkNameAdmin = (name, id) => {
    return (dispatch, getState) => {
        const newState = drinkService.editDrinkNameInState(getState(), name, id);
        dispatch(editDrinkNameInState(newState));
    }
}

const getDrinks = (payload) => {
    return {
        type: REQUEST_DRINK,
        payload
    };
}


    const addNewDrink = (name, cost, count, imageFile) => {
    return async (dispatch, getState) => {
        try {
            const newState = JSON.parse(JSON.stringify(getState().drink));
            const drink = new FormData();
            drink.append('name', name);
            drink.append('cost', cost);
            drink.append('count', count);
            drink.append('ImageFile', imageFile);
            const url = 'drink'
            let response = await fetch(url, {
                method: 'POST',
                body: drink
            });
            let result = await response.json();
            dispatch({ type: ADD_NEW_DRINK, payload: { drinks: [...newState.drinks, result], loading: false } });


        } catch (err) {
            console.error("Error while saving drinks:", err)
        }
    }
}

const fetchDrinks = () => {
    return async (dispatch, getState) => {
        try {
            const url = 'drink/GetAll';
            let request = await fetch(url);
            let jsonResult = await request.json();
            dispatch(getDrinks(jsonResult));
        } catch (err) {
            console.error("Error when receiving drinks: ", err);
        }
    }
}
const saveDrinkEdit = (data) => {
    return async (dispatch, getState) => {
        const url = "/drink/" + data.id;
        const form = new FormData();
        form.append('id', data.id);
        form.append('name', data.name);
        form.append('status', data.status);
        form.append('cost', data.cost);
        form.append('count', data.count);
        form.append('imageFile', data.imageFile);
        try {
            await fetch(url, {
                method: 'PUT',
                body: form
            });
            
            dispatch({ type: SAVE_DRINK_EDIT });
            dispatch(fetchDrinks());
        } catch (e) {
            console.error(e);
        }
    }
}
const deleteDrink = (id) => {
    return async (dispatch, getState) => {
        try {
            const newState = JSON.parse(JSON.stringify(getState().drink));
            const drinks = newState.drinks;
            console.log(newState)
            if (!drinks) {
                dispatch({ type: DELETE_DRINK, payload: newState });
            }

            const idx = drinks.findIndex((el) => el.id === id);


            if (idx !== undefined) {

                const url = 'drink/' + id;
                await fetch(url, {
                    method: "DELETE",
                });

                dispatch({
                    type: DELETE_DRINK, payload: {
                        drinks: [
                            ...drinks.slice(0, idx),
                            ...drinks.slice(idx + 1)
                        ],
                        loading: false
                    }
                }  );  
            }
        }catch (e) {
        console.error("Error when deleting drink", e);
    }
}
}
export {
    addDrink, fetchDrinks, getStatus,
    deleteDrink, minusDrinkAdmin, plusDrinkAdmin,
    minusCostDrinkAdmin, plusCostDrinkAdmin, editDrinkNameAdmin,
    toggleDrinkStatus, addNewDrink, saveDrinkEdit, uploadFile
};