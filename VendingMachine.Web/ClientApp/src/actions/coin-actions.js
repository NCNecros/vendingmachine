﻿import '../app/types';
import { ADD_COIN, REQUEST_COIN, ADMIN_INC_COIN, ADMIN_DEC_COIN, ADMIN_TOGGLE_STATUS } from '../app/types';
import OrderService from "../services/order-service";
const orderService = new OrderService();


const addCoin = (id) => {
    return (dispatch, getState)=> {
        dispatch(addCoinToOrder(getState(),id));
    };
}


const addCoinToOrder=(state,id) => {
    return {
        type: ADD_COIN,
        payload: orderService.addCoin(state, id)
    }
}


const getCoins = (payload) => {
    console.log("getCoins", payload);
    return {
        type: REQUEST_COIN,
        payload: payload
    }
}

const adminIncCoin=(id) => {
    return async (dispatch, getState) => {
        const state = getState().coin;
        console.log("adminIncCoin",state);
        const coin = state.coins.find((el) => el.id === id);
        coin.count++;
        const status = coin.status;
        const url = "/coin/" + id;
        try {
            await fetch(url, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify({ id, status, count:coin.count })
            });
           

            dispatch({ type: ADMIN_INC_COIN,payload:state });
        } catch (e) {
            console.error(e);
        }
    }
}
const adminDecCoin=(id) => {
    return async (dispatch, getState) => {
        const state = getState().coin;
        console.log("adminIncCoin",state);
        const coin = state.coins.find((el) => el.id === id);
        coin.count--;
        const status = coin.status;
        const url = "/coin/" + id;
        try {
            await fetch(url, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify({ id, status, count:coin.count })
            });

            dispatch({ type: ADMIN_DEC_COIN,payload:state });
        } catch (e) {
            console.error(e);
        }
    }
}
const adminToggleStatus =(id) => {
    return async (dispatch, getState) => {
        const state = getState().coin;
        console.log("adminIncCoin",state);
        const coin = state.coins.find((el) => el.id === id);
         coin.status = !coin.status;
        const url = "/coin/" + id;
        try {
            let response = await fetch(url, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify({ id, status:coin.status, count:coin.count })
            });
            await response.json();
            dispatch({ type: ADMIN_TOGGLE_STATUS,payload:state });
        } catch (e) {
            console.error(e);
        }
    }
}





function fetchCoins() {
    return async (dispatch, getState) => {
        try {
            let response = await fetch('coin/GetAll');
            let jsonResult = await response.json();
            dispatch(getCoins(jsonResult));
        } catch (err) {
            console.error("Error when receiving coins: ", err);
        }
    }
}




export { addCoin, fetchCoins, adminToggleStatus, adminIncCoin, adminDecCoin };