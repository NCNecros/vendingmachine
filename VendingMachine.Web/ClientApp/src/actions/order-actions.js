﻿import {
    SEND_ORDER
} from "../app/types";


const sendOrder = () => {
    return async (dispatch, getState) => {
        console.log("Send Order");
        const url = "/order";
        try {
            let response = await fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify(getState().order)
            });
            let result = await response.json();
            dispatch(await resetOrder(result));
        } catch (e) {
            console.error(e);
        }
    }
}

const resetOrder = (payload) => {
    return {
        type: SEND_ORDER,
        payload: payload
    }
}

export { sendOrder };