﻿
class DrinkService {
    async getDrinks() {
        const url = "coins/GetAll";
        const response = await fetch(url);
        const result = await response.json();
        return result;
    }
    toggleDrinkStatus(state, id) {
        const newState = JSON.parse(JSON.stringify(state));
        const drinks = newState.drink;
        const item = drinks.drinks.find((el) => el.id === id);
        item.status = !item.status;
        return drinks;

    }
    minusDrinkAdmin (state, id) {
        const newState = JSON.parse(JSON.stringify(state));
        const drinks = newState.drink;
        const item = drinks.drinks.find((el) => el.id === id);
        item.count--;
        return drinks;
    }
    plusDrinkAdmin(state, id) {
        const newState = JSON.parse(JSON.stringify(state));
        const drinks = newState.drink;
        const item = drinks.drinks.find((el) => el.id === id);
        item.count++;
        return drinks;
    }
    editDrinkNameInState(state,name, id) {
        const newState = JSON.parse(JSON.stringify(state));
        const drinks = newState.drink;
        const item = drinks.drinks.find((el) => el.id === id);
        item.name=name;
        return drinks;
    }
    minusCostDrinkAdmin(state, id) {
        const newState = JSON.parse(JSON.stringify(state));
        const drinks = newState.drink;
        const item = drinks.drinks.find((el) => el.id === id);
        item.cost--;
        return drinks;
    }
    plusCostDrinkAdmin(state, id) {
        const newState = JSON.parse(JSON.stringify(state));
        const drinks = newState.drink;
        const item = drinks.drinks.find((el) => el.id === id);
        item.cost++;
        return drinks;
    }
    minusDrink(state, id) {
        const newState = JSON.parse(JSON.stringify(state));
        const drinks = newState.drink;
        const item = drinks.drinks.find((el) => el.id === id);
        item.count--;
        return drinks;
    }

    getStatus(state, id) {
        const newState = JSON.parse(JSON.stringify(state));
        const drinks = newState.drink;
        if (!drinks) {
            return newState;
        }
        const orderBalance = newState.order.balance;
        const item = drinks.drinks.find((el) => el.id === id);
        console.log(item, orderBalance);
        if (orderBalance < item.cost || item.count <= 0) {
            console.log(false);
            item.status = false;
        } else {
            console.log(true);
            item.status = true;
        }

                return drinks;
    }
    
}

export default DrinkService;