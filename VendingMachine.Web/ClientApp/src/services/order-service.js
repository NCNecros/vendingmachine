﻿
class OrderService {

     addDrink(state, id){
         const currentState = JSON.parse(JSON.stringify(state));
         const { drink, order } = currentState;
         const drinkInOrder = order.drinks.find((el) => el.id === id);
         const selectedDrink = drink.drinks.find((el) => el.id === id);
         if (drinkInOrder === undefined) {
             order.drinks = [...order.drinks, selectedDrink];
             selectedDrink.count=1;
         } else {
             drinkInOrder.count++;
         }
         order.balance -= selectedDrink.cost;
         

         return order;
    }

     addCoin(state, id) {
         const currentState = JSON.parse(JSON.stringify(state));
         const { coin, order } = currentState;
         const coinInOrder = order.coins.find((el) => el.id === id);
         const selectedCoin = coin.coins.find((el) => el.id === id);
         if (coinInOrder === undefined) {
             order.coins = [...order.coins, selectedCoin];
             selectedCoin.count = 1;
             order.cost += selectedCoin.cost;
         }else{
             coinInOrder.count++;
             order.cost += selectedCoin.cost;
         }
         order.balance += selectedCoin.cost;

         return order;
     }
}
export default OrderService;