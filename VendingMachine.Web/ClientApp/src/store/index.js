﻿import { composeWithDevTools } from "redux-devtools-extension";
import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import { logger } from 'redux-logger';
import rootReducer from '../reducers';

const store = createStore(
    rootReducer,
    composeWithDevTools(
        applyMiddleware(thunk,logger)
    )
);

export default store;
