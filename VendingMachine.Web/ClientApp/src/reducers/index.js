﻿import { combineReducers } from 'redux';
import { coinReducer } from './coin_reducer';
import { drinkReducer } from './drink_reducer';
import { orderReducer } from './order_reducer';

export default combineReducers({
    coin: coinReducer,
    drink: drinkReducer,
    order: orderReducer
})

