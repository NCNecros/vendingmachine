﻿import { ADD_ORDER, ORDER_ADD_DRINK, ADD_COIN, SEND_ORDER } from "../app/types";

const defaultState = {
    drinks: [],
    coins: [],
    cost: 0,
    balance: 0,
    rest: []
};

const orderReducer = (state = defaultState, action) => {
    switch (action.type) {
        case ADD_COIN:
        {
            return action.payload;
        }
        case ORDER_ADD_DRINK: {
            return action.payload;
        }

        case SEND_ORDER: {
            return {
                ...defaultState,
                rest: action.payload
            };
        }
        default:
            return state;
    }
}

export { orderReducer };
