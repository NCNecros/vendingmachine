﻿import {
    ADD_DRINK, DELETE_DRINK, DISABLE_DRINK,
    ENABLE_DRINK, MINUS_DRINK, REQUEST_DRINK,
    CHECK_DRINK_STATUS, MINUS_DRINK_ADMIN,
    PLUS_DRINK_ADMIN, MINUS_COST_DRINK_ADMIN,
    PLUS_COST_DRINK_ADMIN, EDIT_NAME_IN_STATE,
    TOGGLE_DRINK_STATUS, ADD_NEW_DRINK, SAVE_DRINK_EDIT
} from "../app/types";

const defaultState = {
    drinks: [],
    loading: true
}
const drinkReducer = (state = defaultState, action) => {
    switch (action.type) {
        case ADD_DRINK:
            {
                return action.payload;
            }
        case ADD_NEW_DRINK:
            {
                return action.payload;
            }
        case DISABLE_DRINK:
            {
                console.log("Disable drink ", action.payload);
                break;
            }
        case ENABLE_DRINK:
            {
                console.log("Enable drink ", action.payload);
                break;
            }
        case REQUEST_DRINK:
            {
                return {
                    ...state,
                    drinks: action.payload,
                    loading: false
                }
            }
        case MINUS_DRINK_ADMIN:
            {
                return action.payload;
            }
        case PLUS_DRINK_ADMIN:
            {
                return action.payload;
            }
        case MINUS_COST_DRINK_ADMIN:
            {
                return action.payload;
            }
        case SAVE_DRINK_EDIT:
            {
                return state;
            }
        case PLUS_COST_DRINK_ADMIN: {
            return action.payload;
        }
        case EDIT_NAME_IN_STATE: {
            return action.payload;
        }

        case TOGGLE_DRINK_STATUS: {
            return action.payload;
        }
        case MINUS_DRINK: {
            return action.payload;
        }
        case DELETE_DRINK: {
            return action.payload;
        }
        case CHECK_DRINK_STATUS: {
            return {
                ...state,
                ...action.payload
            }
        }
        default:
            return state;
    }
}

export { drinkReducer };