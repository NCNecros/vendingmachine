﻿import { ADD_COIN, REQUEST_COIN, ADMIN_INC_COIN, ADMIN_DEC_COIN, ADMIN_TOGGLE_STATUS } from "../app/types";

const defaultState = {
    coins: [],
    loading: true
}
function coinReducer(state = defaultState, action) {

    switch (action.type) {

        case REQUEST_COIN:
            {
                return {
                    ...state,
                    coins: action.payload, loading: false
                };
            }
        case ADMIN_INC_COIN:
        {
              return({ coins: [...action.payload.coins], loading:false });

        }
        case ADMIN_DEC_COIN:
            {
            return({coins: [...action.payload.coins], loading:false});
            }
            case ADMIN_TOGGLE_STATUS:
            {
                return({coins: [...action.payload.coins], loading:false});
            }
        default:
            return state;
    }
}

export { coinReducer };