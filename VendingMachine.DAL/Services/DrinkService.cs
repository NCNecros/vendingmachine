﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using VendingMachine.Models.Context;
using VendingMachine.Models.Dto;
using VendingMachine.Models.Models;

namespace VendingMachine.DAL.Services
{
    public interface IDrinkService
    {
        Task<Drink> SaveAsync(Drink drink);
        IEnumerable<Drink> GetAll();
        Task<Drink> FindAsync(int id);
        Task<Drink> AddNewAsync(Drink drink);
        Task DeleteAsync(int id);

        Task<Boolean> DecreaseDrinkAsync(int id, int count);
        Task<Boolean> IncreaseDrinkAsync(int id, int count);
        Task<Boolean> ChangeState(int id, bool isEnabled);
        Task<Boolean> ChangeCountAsync(IList<OrderDrinkDto> drinks);
    }

    public class DrinkService : IDrinkService
    {
        private readonly VmContext _db;

        public DrinkService(VmContext db)
        {
            _db = db;
        }
        public async Task<Drink> AddAsync(Drink drink)
        {
            try
            {
                _db.Attach(drink);
                await _db.SaveChangesAsync();
                return drink;
            }
            catch
            {
                return null;
            }
        }
        public async Task<Drink> SaveAsync(Drink drink)
        {
            try
            {
                _db.Entry<Drink>(drink).State = EntityState.Modified;
                await _db.SaveChangesAsync();
                return drink;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public async Task<Drink> FindAsync(int id)
        {
            return await _db.Drinks.FindAsync(id);
        }
        public IEnumerable<Drink> GetAll()
        {
            return _db.Drinks;
        }

        public async Task<Boolean> DecreaseDrinkAsync(int id, int count)
        {
            var drink = await _db.Drinks.FindAsync(id);
            if (drink != null)
            {
                drink.Count -= count;
            }
            else
            {
                return false;
            }
            _db.Entry(drink).State = EntityState.Modified;
            await _db.SaveChangesAsync();
            return true;
        }

        public async Task<Boolean> IncreaseDrinkAsync(int id, int count)
        {
            var drink = await _db.Drinks.FindAsync(id);
            if (drink != null)
            {
                drink.Count += count;
            }
            else
            {
                return false;
            }
            _db.Entry(drink).State = EntityState.Modified;
            await _db.SaveChangesAsync();
            return true;
        }

        public async Task<bool> ChangeState(int id, bool isEnabled)
        {
            var drink = await _db.Drinks.FindAsync(id);
            if (drink != null)
            {
                drink.Status = isEnabled;
                _db.Entry(drink).State = EntityState.Modified;
                await _db.SaveChangesAsync();
                return true;
            }

            return false;
        }

        public async Task<bool> ChangeCountAsync(IList<OrderDrinkDto> drinks)

        {
            foreach (OrderDrinkDto drink in drinks)
            {

                var drinkFromDB = await _db.Drinks.FirstAsync(e => e.Id == drink.Id);
                if (drinkFromDB == null) continue;
                if (drinkFromDB.Count > drink.Count)
                {
                    drinkFromDB.Count -= drink.Count;
                    _db.Entry(drinkFromDB).State = EntityState.Modified;
                    await _db.SaveChangesAsync();
                }
                else
                {
                    return false;
                }
            }
            return true;

        }

        public async Task<Drink> AddNewAsync(Drink drink)
        {
            await _db.Drinks.AddAsync(drink);
            await _db.SaveChangesAsync();
            return drink;

        }

        public async Task DeleteAsync(int id)
        {
            var drink = await _db.Drinks.FindAsync(id);
            if (drink != null)
            {
                _db.Drinks.Remove(drink);
                _db.Entry(drink).State=EntityState.Deleted;
                await _db.SaveChangesAsync();
            }
        }

    }
}
