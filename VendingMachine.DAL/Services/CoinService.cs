﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using VendingMachine.Models.Context;
using VendingMachine.Models.Models;

namespace VendingMachine.DAL.Services
{
    public interface ICoinService
    {
        IEnumerable<Coin> GetAll();
        Task<Coin> FindAsync(int Id);
        Task<bool> SaveAsync(Coin coin);
        Task<Coin> AddAsync(Coin coin);
        Task<bool> DecreaseCoinAsync(int id, int count);
        Task<bool> IncreaseCoinAsync(int id, int count);
        Task ChangeCoinAsync(IList<OrderCoinDto> coins);
    }

    public class CoinService : ICoinService
    {
        private readonly VmContext _db;

        public CoinService(VmContext db)
        {
            _db = db;
        }

        public IEnumerable<Coin> GetAll()
        {
            return _db.Coins;
        }

        public async Task<Coin> FindAsync(int Id)
        {
            return await _db.Coins.FindAsync(Id);
        }

        public async Task<bool> SaveAsync(Coin coin)
        {
            try
            {
                _db.Entry<Coin>(coin).State = EntityState.Modified;
                await _db.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public async Task<Coin> AddAsync(Coin coin)
        {
            _db.Attach<Coin>(coin);
            await _db.SaveChangesAsync();
            return coin;
        }
        public async Task<Boolean> IncreaseCoinAsync(int id, int count)
        {
            var coin = await _db.Coins.FindAsync(id);
            try {
                coin.Count+=count;
                _db.Entry(coin).State=EntityState.Modified;
                return true;
            }catch{
                return false;
            }
        }

        public async Task<Boolean> DecreaseCoinAsync(int id, int count)
        {
            var coin = await _db.Coins.FindAsync(id);
            try {
                coin.Count-=count;
                _db.Entry(coin).State=EntityState.Modified;
                return true;
            }catch{
                return false;
            }
        }

        public async Task ChangeCoinAsync(IList<OrderCoinDto> coins)
        {
            foreach(OrderCoinDto coin in coins)
            {
                var coinFromDB = await _db.Coins.FindAsync(coin.Id);
                coinFromDB.Count += coin.Count;
                _db.Entry(coinFromDB).State = EntityState.Modified;
            }
            await _db.SaveChangesAsync();
        }
    }
}