﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VendingMachine.Models.Context;
using VendingMachine.Models.Models;

namespace VendingMachine.DAL.Services
{
    public interface IOrderService
    {
        Task<Order> SaveAsync(Order order, int summa);
    }
    public class OrderService : IOrderService
    {
        private readonly VmContext _db;

        public OrderService(VmContext db)
        {
            _db = db;
        }

        public async Task<Order> SaveAsync(Order order, int summa)
        {
            _db.Attach(order);
            var savedOrder = await _db.SaveChangesAsync();
            return order;
        }
    }
}
