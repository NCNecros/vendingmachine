using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

using VendingMachine.Models.Configurations;
using VendingMachine.Models.Models;

namespace VendingMachine.Models.Context
{
    public class VmContext : DbContext
    {
        private const string SchemaName = "vm";
        private readonly IConfiguration configuration;

        public VmContext(DbContextOptions<VmContext> options, IConfiguration configuration)
            : base(options)
        {
            this.configuration = configuration;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(SchemaName);
            modelBuilder.ApplyConfiguration(new DrinkConfiguration());
            modelBuilder.ApplyConfiguration(new CoinConfiguration());
            }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(configuration.GetConnectionString("VmDb"),
                b => b.MigrationsAssembly("VendingMachine.Web"));
        }

        public DbSet<Coin> Coins { get; set; }
        public DbSet<Drink> Drinks { get; set; }
        public DbSet<Order> Orders { get; set; }
    }
}