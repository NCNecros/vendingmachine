﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VendingMachine.Models.Models;

namespace VendingMachine.Models.Dto
{
    public class OrderRequestDto
    {
        public IList<OrderCoinDto> Coins {get;set;}
        public IList<OrderDrinkDto> Drinks { get; set; }
        public int Balance { get; set; }
    }
}
