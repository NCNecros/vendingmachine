﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine.Models.Dto
{
    public class OrderDrinkDto
    {
        public int Id { get; set; }
        public int Count { get; set; }
        public int Cost { get; set; }
    }
}
