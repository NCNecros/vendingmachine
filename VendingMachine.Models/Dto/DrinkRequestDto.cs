﻿using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine.Models.Dto
{
    public class DrinkRequestDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
public int Cost { get; set; }
public bool Status { get; set; }
        public int Count { get; set; }
        public IFormFile ImageFile { get; set; }
    }
}
