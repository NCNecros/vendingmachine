﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine.Models.Dto
{
    public class OrderResponseDto
    {
        public bool Result { get; set; }
        public List<CoinResponseDto> Coins { get; set; }
    }
}
