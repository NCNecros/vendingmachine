﻿namespace VendingMachine.Models.Dto
{
    public class CoinRequestDto
    {
        public int Id { get; set; }
        public bool Status { get; set; }
        public int Count { get; set; }
    }
}