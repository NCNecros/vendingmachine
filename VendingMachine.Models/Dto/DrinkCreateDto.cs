﻿using Microsoft.AspNetCore.Http;

namespace VendingMachine.Models.Dto
{
	public class DrinkCreateDto
	{
		public string Name { get; set; }
		public int Cost {get;set;}
		public int Count { get; set; }
		public IFormFile ImageFile { get; set; }
	}
}