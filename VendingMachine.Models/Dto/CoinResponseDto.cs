﻿namespace VendingMachine.Models.Dto
{
    public class CoinResponseDto
    {
        public int Id { get; set; }
        public int CoinType { get; set; }
        public bool Status { get; set; }
        public int Count { get; set; }
        public int Cost { get; set; }

    }
}