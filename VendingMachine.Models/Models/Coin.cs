﻿using System.ComponentModel.DataAnnotations;

namespace VendingMachine.Models.Models
{
    public class Coin
    {
        [Key]
        public int Id { get; set; }
        public int Count { get; set; }
        public int Cost { get; set; }
        public bool Status {get;set;}

    }
}