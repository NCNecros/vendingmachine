﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace VendingMachine.Models.Models
{
    public class Order
    {
        [Key]
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public int Summa { get; set; }
    }
}