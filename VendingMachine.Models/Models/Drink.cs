﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace VendingMachine.Models.Models
{
    public class Drink
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }
        public int Count { get; set; }
        public int Cost { get; set; }
        public bool Status { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}