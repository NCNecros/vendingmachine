﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

using VendingMachine.Models.Dto;
using VendingMachine.Models.Models;

namespace VendingMachine.Models.Mapper
{
   public class DrinkMappingProfile : Profile
    {
        public DrinkMappingProfile()
        {
            CreateMap<Drink, DrinkCreateDto>();
            CreateMap<Drink, DrinkResponseDto>();
            CreateMap<DrinkResponseDto, Drink>();
            CreateMap<DrinkCreateDto, Drink>();
            CreateMap<DrinkRequestDto, Drink>();
        }
    }
}
