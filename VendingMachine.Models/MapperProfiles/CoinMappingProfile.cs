﻿using AutoMapper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VendingMachine.Models.Dto;
using VendingMachine.Models.Models;

namespace VendingMachine.Models.MapperProfiles
{
    class CoinMappingProfile : Profile
    {
        public CoinMappingProfile()
        {
            CreateMap<Coin, CoinRequestDto>();
            CreateMap<Coin, CoinResponseDto>();
            CreateMap<CoinRequestDto, CoinResponseDto>();
        }
    }
}
