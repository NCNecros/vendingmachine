﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VendingMachine.Models.Models;

namespace VendingMachine.Models.Configurations
{
    class CoinConfiguration : IEntityTypeConfiguration<Coin>
    {
        public void Configure(EntityTypeBuilder<Coin> builder)
        {
            builder.HasData(
                new Coin { Id = 1, Status = true, Count = 10, Cost = 1},
                new Coin { Id = 2, Status = true, Count = 10 ,Cost = 2},
                new Coin { Id = 3, Status = false, Count = 10 , Cost = 5},
                new Coin { Id = 4, Status = true, Count = 10 ,Cost = 10}
            );
        }
    }
}
