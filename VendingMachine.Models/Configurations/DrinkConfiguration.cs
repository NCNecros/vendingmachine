using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VendingMachine.Models.Models;

namespace VendingMachine.Models.Configurations
{
	public class DrinkConfiguration : IEntityTypeConfiguration<Drink>
	{
		public void Configure(EntityTypeBuilder<Drink> builder)
		{
			builder.Property(d => d.Name).HasMaxLength(500);
			builder.Property(d => d.Picture).HasMaxLength(1024);

			builder.HasIndex(p => p.Name).IsUnique();
			builder.HasData(
				new Drink { Id = 1, Name = "Coca-cola", Count = 10 , Cost = 3, Status = true},
				new Drink { Id = 2, Name = "Fanta", Count = 10 ,Cost = 5, Status = true},
				new Drink { Id = 3, Name = "Sprite", Count = 10 , Cost = 4, Status = false},
				new Drink { Id = 4, Name = "Pepsi", Count = 10 , Cost = 7, Status = true}
				);

		}
	}
}